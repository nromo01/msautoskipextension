The Morgan Stanley Access Account Page Skipper will automatically skip the landing page that users redirect to after logging into [Morgan Stanley Stock Plan Connect](https://stockplanconnect.morganstanley.com/)

For the sake of simplicity the extension will just click the `Remind Me Later` element at the bottom of the page.

Steps for Installing Chrome Extension:

1. Clone or Download this repository.

    a. If you Download the repository, navigate to your downloads to the `.zip` file you just downloaded. Unzip the folder
    and rename it to `msautoskipextension`.
    

2. Open a Chrome Window.

3. Type in the address bar: `chrome://extensions`

4. In the top right of the window you should see a button for `Developer Mode`. Enable this.

5. In the top left of the window press `Load Unpacked`. This will open a system window asking for the root directory.

6. Click on the directory/folder of the repository downloaded above. In this case it would be `masautoskipextension`.

7. All Done! The next time you log into MorganStanley the Access Direct Account page will be skipped for you.