let numRetries = 5
const parentElementOfButtonID = "spbaInterRemindMeLater"
function waitForHTMLButtonToLoad() {
    try {
        document.getElementById(parentElementOfButtonID).children[0].click()
    }
    catch (e) {
        //Lazily retry
        if (numRetries-- === 0) {
            return
        }
    }
    setTimeout(waitForHTMLButtonToLoad, 1000)
}
waitForHTMLButtonToLoad()
